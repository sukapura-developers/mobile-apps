import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldState =
      new GlobalKey<ScaffoldState>();

  _callSnackbar() {
    _scaffoldState.currentState.showSnackBar(SnackBar(
      content: Text('Panggil aku'),
    ));
  }

  _callBottomsheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Wrap(
                  children: <Widget>[
                    new ListTile(
                        leading: new Icon(Icons.music_note),
                        title: new Text('Music'),
                        onTap: () => {}),
                    new ListTile(
                      leading: new Icon(Icons.videocam),
                      title: new Text('Video'),
                      onTap: () => {},
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      body: ListView(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.account_circle),
            title: Text("Profile"),
            trailing: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.keyboard_arrow_right)),
          ),
          ListTile(
            leading: Icon(Icons.aspect_ratio),
            title: Text("Show Snackbar"),
            trailing: IconButton(
                onPressed: () {
                  _callSnackbar();
                },
                icon: Icon(Icons.keyboard_arrow_right)),
          ),
          ListTile(
            leading: Icon(Icons.add_to_home_screen),
            title: Text(
              "Bottom Sheet",
              style: TextStyle(fontFamily: 'Merriweather'),
            ),
            trailing: IconButton(
                onPressed: () {
                  _callBottomsheet(context);
                },
                icon: Icon(Icons.keyboard_arrow_right)),
          ),
          SnackbarPage(),
        ],
      ),
    );
  }
}

class SnackbarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text("Show Snack bar"),
      onPressed: () {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text("Data Snackbar"),
          backgroundColor: Colors.blueAccent,
          action: SnackBarAction(
            onPressed: () {},
            label: 'Undo',
            textColor: Colors.white,
          ),
        ));
      },
    );
  }
}
