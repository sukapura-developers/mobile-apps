import 'package:flutter/material.dart';

class Accounts extends StatefulWidget {
  @override
  _AccountsState createState() => _AccountsState();
}

class _AccountsState extends State<Accounts> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Container(
            height: 25,
            child: Image.asset('images/sukapura.png'),
          ),
          actions: <Widget>[
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.search),
              color: Colors.black45,
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.message),
              color: Colors.black45,
            )
          ],
          backgroundColor: Colors.white,
          elevation: 1,
          centerTitle: false,
        ),
        body: Container(
          child: ListView(
            children: const <Widget>[
              ListTile(
                leading: Icon(Icons.grain),
                title: Text("Rewards"),
                trailing: IconButton(
                  icon: Icon(Icons.keyboard_arrow_right),
                ),
              ),
              ListTile(
                leading: Icon(Icons.layers),
                title: Text("Terjadwal"),
                trailing: IconButton(
                  icon: Icon(Icons.keyboard_arrow_right),
                ),
              ),
              ListTile(
                leading: Icon(Icons.person_pin_circle),
                title: Text("Langganan"),
                trailing: IconButton(
                  icon: Icon(Icons.keyboard_arrow_right),
                ),
              ),
              ListTile(
                leading: Icon(Icons.account_circle),
                title: Text("Points"),
                trailing: IconButton(
                  icon: Icon(Icons.keyboard_arrow_right),
                ),
              ),
              ListTile(
                leading: Icon(Icons.send),
                title: Text("Kasih Masukan"),
                trailing: IconButton(
                  icon: Icon(Icons.keyboard_arrow_right),
                ),
              ),
              ListTile(
                leading: Icon(Icons.history),
                title: Text("Riwayat Terakhir"),
                trailing: IconButton(
                  icon: Icon(Icons.keyboard_arrow_right),
                ),
              ),
              ListTile(
                leading: Icon(Icons.help),
                title: Text("Pusat Bantuan"),
                trailing: IconButton(
                  icon: Icon(Icons.keyboard_arrow_right),
                ),
              ),
              ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text("Logout"),
                trailing: IconButton(
                  icon: Icon(Icons.keyboard_arrow_right),
                ),
              ),
            ],
          ),
        ));
  }
}
